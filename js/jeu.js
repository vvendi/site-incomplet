var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;

document.getElementById("game").appendChild(canvas);

var bgReady = false;
var bgImage = new Image();
bgImage.onload = function ()
{
    bgReady = true;
};
bgImage.src = "img/background.png";

var herosReady = false;
var herosImage = new Image();
herosImage.onload = function ()
{
    herosReady = true;
};
herosImage.src = "img/hero.png";

var monstreReady = false;
var monstreImage = new Image();
monstreImage.onload = function ()
{
    monstreReady = true;
};
monstreImage.src = "img/monster.png";

var heros =
{
    speed: 256, // vitesse en pixels par seconde
    x: 0,
    y: 0
};

var monstre =
{
    x: 0,
    y: 0
};

var monstresAttrapes = 0;

var touchesAppuyees = {};

addEventListener("keydown", function (e)
{
    if([' ', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'].indexOf(e.key) > -1) {
        e.preventDefault();
        touchesAppuyees[e.key] = true;
    }
}, false);

addEventListener("keyup", function (e)
{
    if([' ', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'].indexOf(e.key) > -1) {
        delete touchesAppuyees[e.key];
    }
}, false);

var reset = function ()
{    
    heros.x = canvas.width / 2 - 16,
    heros.y = canvas.height / 2 - 16
    // Faire apparaître un monstre au hasard
    monstre.x = 32 + (Math.random() * (canvas.width - 96));
    monstre.y = 32 + (Math.random() * (canvas.height - 96));

    if(heros.x <= (monstre.x + 32)
        && monstre.x <= (heros.x + 32)
        && heros.y <= (monstre.y + 32)
        && monstre.y <= (heros.y + 32))
    {
        reset();
    }
};

var update = function (modifier)
{
    if ('ArrowUp' in touchesAppuyees)
    {
        heros.y -= heros.speed * modifier;
    }

    if ('ArrowDown' in touchesAppuyees)
    {
        heros.y += heros.speed * modifier;
    }

    if ('ArrowLeft' in touchesAppuyees)
    {
        heros.x -= heros.speed * modifier;
    }

    if ('ArrowRight' in touchesAppuyees)
    {
        heros.x += heros.speed * modifier;
    }

    // Y a-t-il contact ?
    if(heros.x <= (monstre.x + 32)
        && monstre.x <= (heros.x + 32)
        && heros.y <= (monstre.y + 32)
        && monstre.y <= (heros.y + 32))
    {
        ++monstresAttrapes;
        reset();
    }
};

var render = function ()
{
    if (bgReady)
    {
        ctx.drawImage(bgImage, 0, 0);
    }

    if (herosReady)
    {
        ctx.drawImage(herosImage, heros.x, heros.y);
    }

    if (monstreReady)
    {
        ctx.drawImage(monstreImage, monstre.x, monstre.y);
    }

    // Score
    ctx.fillStyle = "rgb(250, 250, 250)";
    ctx.font = "24px Helvetica";
    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText( " Points : " + monstresAttrapes, 32, 32);
};

var main = function ()
{
    var now = Date.now();
    var delta = now - then;
    update(delta / 1000);
    render();
    then = now;
};

reset();
var then = Date.now();
setInterval(main, 1); // Executer aussi vite que possible
